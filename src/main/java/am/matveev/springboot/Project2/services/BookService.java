package am.matveev.springboot.Project2.services;


import am.matveev.springboot.Project2.models.Book;
import am.matveev.springboot.Project2.models.Person;
import am.matveev.springboot.Project2.repositories.BooksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BookService{


     private final BooksRepository booksRepository;

     @Autowired
    public BookService(BooksRepository booksRepository){
        this.booksRepository = booksRepository;
    }
    @Transactional(readOnly = true)
    public List<Book> findAll(boolean sort){
         if(sort){
             return booksRepository.findAll(Sort.by("year"));
         }else {
             return booksRepository.findAll();
         }
    }

    @Transactional(readOnly = true)
    public List<Book> findWithPagination(Integer page,Integer size,boolean sort){
         if(sort){
             return booksRepository.findAll(PageRequest.of(page,size,Sort.by("year"))).getContent();
         }else {
             return booksRepository.findAll(PageRequest.of(page,size)).getContent();
         }
    }

    @Transactional(readOnly = true)
    public Book findOne(int id){
        Optional<Book> foundBook = booksRepository.findById(id);
        return foundBook.orElse(null);
    }

    @Transactional
    public Person getBookOwner(int id){
         return booksRepository.findById(id).map(Book::getOwner).orElse(null);
    }

    @Transactional
    public List<Book> searchByTitle(String query){
         return booksRepository.findByTitleStartingWith(query);
    }

    @Transactional
    public void save(Book book){
         booksRepository.save(book);
    }

    @Transactional
    public void update(int id,Book updatedBook){
         Book bookToBeUpdated = booksRepository.findById(id).get();
         updatedBook.setId(id);
         bookToBeUpdated.setOwner(updatedBook.getOwner());
         booksRepository.save(bookToBeUpdated);
    }

    @Transactional
    public void delete(int id){
         booksRepository.deleteById(id);
    }

    @Transactional
    public void release(int id){
         booksRepository.findById(id).ifPresent(book -> {
             book.setOwner(null);
             book.setTakenAt(null);
         });
    }

    @Transactional
    public void assign(int id,Person selectedPerson){
         booksRepository.findById(id).ifPresent(book -> {
             book.setOwner(selectedPerson);
             book.setTakenAt(new Date());
         });
    }

}

package am.matveev.springboot.Project2.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;


import java.util.List;

@Entity
@Table(name = "Person")
@Getter
@Setter
public class Person{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "fullName")
    @NotEmpty(message = "Поле не должно быть пустым")
    @Size(min = 3,max = 100,message = "ФИО должно быть между 3 и 100 символов")
    private String fullName;

    @Column(name = "year_of_birth")
    @Min(value = 1940,message = "Год рождения должен быть не меньше 1940")
    private int yearOfBirth;

    @OneToMany(mappedBy = "owner",fetch = FetchType.EAGER)
    private List<Book> books;

    public Person(){

    }

    public Person(String fullName, int yearOfBirth){
        this.fullName = fullName;
        this.yearOfBirth = yearOfBirth;
    }

    @Override
    public String toString(){
        return "Person{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", yearOfBirth=" + yearOfBirth +
                '}';
    }
}

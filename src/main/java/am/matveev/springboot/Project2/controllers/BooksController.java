package am.matveev.springboot.Project2.controllers;


import am.matveev.springboot.Project2.models.Book;
import am.matveev.springboot.Project2.models.Person;
import am.matveev.springboot.Project2.services.BookService;
import am.matveev.springboot.Project2.services.PeopleService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/books")
public class BooksController{

    private final BookService bookService;
    private final PeopleService peopleService;

    @Autowired
    public BooksController(BookService bookService, PeopleService peopleService){
        this.bookService = bookService;
        this.peopleService = peopleService;
    }

    @GetMapping()
    public String allBooks(Model model, @RequestParam(value = "page",required = false)Integer page,
                           @RequestParam(value = "size",required = false)Integer size,
                           @RequestParam(value = "sort",required = false)boolean sort){
        if(page == null || size == null){
            model.addAttribute("books",bookService.findAll(sort));
        }else {
            model.addAttribute("books",bookService.findWithPagination(page, size, sort));
        }
        return "books/allBooks";
    }

    @GetMapping("/{id}")
    public String showBook(@PathVariable("id") int id, Model model, @ModelAttribute("person") Person person){
        model.addAttribute("book",bookService.findOne(id));

        Person bookOwner = bookService.getBookOwner(id);

        if(bookOwner != null)
            model.addAttribute("owner",bookOwner);
        else
            model.addAttribute("people",peopleService.findAll());
        return "books/showBook";
    }

    @GetMapping("/new")
    public String newBook(@ModelAttribute("book") Book book){
        return "books/new";
    }

    @PostMapping()
    public String create(@ModelAttribute("book")@Valid Book book,
                         BindingResult bindingResult){
        if(bindingResult.hasErrors())
            return "books/new";
        bookService.save(book);
        return "redirect:/books";
    }

    @GetMapping("/{id}/edit")
    public String edit(@PathVariable("id")int id,Model model){
        model.addAttribute("book",bookService.findOne(id));
        return "books/edit";
    }

    @PatchMapping("/{id}")
    public String update(@PathVariable("id")int id,@ModelAttribute("book")@Valid Book book,
                         BindingResult bindingResult){
        if(bindingResult.hasErrors())
            return "books/edit";
        bookService.update(id,book);
        return "redirect:/books";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id")int id){
        bookService.delete(id);
        return "redirect:/books";
    }

    @PatchMapping("/{id}/release")
    public String release(@PathVariable("id")int id){
        bookService.release(id);
        return "redirect:/books/" + id;
    }

    @PatchMapping("/{id}/assign")
    public String assign(@PathVariable("id")int id,@ModelAttribute("person")Person createdPerson){
        bookService.assign(id,createdPerson);
        return "redirect:/books/" + id;
    }

    @GetMapping("/search")
    public String searchBook(){
        return "books/search";
    }

    @PostMapping("/search")
    public String makeSearch(Model model,@RequestParam("query")String query){
        model.addAttribute("books",bookService.searchByTitle(query));
        return "books/search";
    }
}
